import React from "react";


class ShoeForm extends React.Component {
    constructor(){
        super()
        this.state = {
            "manufacturer": "",
            "name": "",
            "color": "",
            "pictureURL": "",
            "bin": "",
            "bins": []
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleURLChange = this.handleURLChange.bind(this)
        this.handleBinChange = this.handleBinChange.bind(this)
    }
    handleManufacturerChange(event) {
        const value = event.target.value
        this.setState({"manufacturer": value})
    }
    handleNameChange(event) {
        const value = event.target.value
        this.setState({"name": value})
    }
    handleBinChange(event) {
        const value = event.target.value
        this.setState({"bin": value})
    }
    handleColorChange(event) {
        const value = event.target.value
        this.setState({"color": value})
    }
    handleURLChange(event) {
        const value = event.target.value
        this.setState({"pictureURL": value})
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        data.picture_url = data.pictureURL
        delete data.pictureURL
        delete data.bins

        const shoeCreateURL = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(shoeCreateURL, fetchConfig)
        if(response.ok){
            const newShoe = await response.json()
            console.log(newShoe)
            const cleared ={
                "manufacturer": "",
                "name": "",
                "color": "",
                "pictureURL": "",
                "bin": "",
            }
        this.setState(cleared)
        }

    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/bins/"
        let response = await fetch(url)
        if(response.ok){
            let data = await response.json();
            this.setState({"bins": data.bins})
        }
    }
    //still need to fill out the full HTML for this input
    render(){
        return(
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={this.handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} value={this.state.color} placeholder="color" type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleURLChange} value={this.state.pictureURL} placeholder="pictureURL" type="text" name="pictureURL" id="pictureURL" className="form-control" />
                    <label htmlFor="pictureURL">Picture url</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleBinChange} value={this.state.bin} name="bin" id="bin" className="form-select">
                    <option>Choose a bin number</option>
                      {this.state.bins.map(bin=> {
                        return (
                            <option key={bin.href} value={bin.href}>{bin.bin_number}</option>
                        )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}

export default ShoeForm
