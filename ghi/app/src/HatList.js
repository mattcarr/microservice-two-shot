import React from 'react';

class HatList extends React.Component {
  constructor () {
    super()
    this.state = {
      "hats": []
    }
  }
async componentDidMount() {
  const url = 'http://localhost:8090/api/hats/'
  let response = await fetch(url)

  if (response.ok) {
    let data = await response.json()
    this.setState({"hats": data.hats})
  }
}

render () {
    return (
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Section Number</th>
            <th>Shelf Number</th>
          </tr>
        </thead>
        <tbody>
          {this.state.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td>{ hat.color}</td>
                <td>{ hat.location.section_number}</td>
                <td>{ hat.location.shelf_number}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
  }
}

  export default HatList;
