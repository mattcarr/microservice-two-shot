from django.urls import path, include
from .api_views import api_list_hats, api_list_Hat, api_show_hat

urlpatterns = [
    path('hats/', api_list_hats, name="api_list_hats"),
    path('locations/', api_list_Hat, name="api_list_Hat"),
    path('hats/<int:pk>/', api_show_hat, name="details_hat"),
    path('hats/<int:pk/', api_show_hat, name='delete_hat'),
    ]
