from django.shortcuts import render
from .models import LocationVO, Hat
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["href", "closet_name", "section_number", "shelf_number"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ['id', 'URL', "fabric", "style_name", "color", "location"]
    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats},encoder=HatListEncoder)

    else:
        content = json.loads(request.body)

        try:
            post_href = content["location"]
            location = LocationVO.objects.get(href=post_href)
            content['location']=location
        except LocationVO.DoesNotExist:
            return JsonResponse({"Message":"Invalid location"}, status=400,)

    hat = Hat.objects.create(**content)
    return JsonResponse(
        hat,
         encoder=HatListEncoder,
         safe=False,
    )

@require_http_methods(["GET"])
def api_list_Hat(request):
    if request.method == "GET":
        locations = LocationVO.objects.all()
        return JsonResponse({"locations": locations}, encoder=LocationVOEncoder)

@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(hat, encoder=HatListEncoder, safe=False)
        except:
            return JsonResponse({"Message": "Invalid ID "})
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})
