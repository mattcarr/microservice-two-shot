from django.db import models

# Create your models here.


class LocationVO(models.Model):
    href = models.CharField(max_length=100, default=None)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hat(models.Model):
    fabric = models.CharField(max_length = 100)
    style_name = models.CharField(max_length = 100)
    color = models.CharField(max_length = 100)
    URL = models.URLField()
    location = models.ForeignKey(LocationVO, related_name="locations", on_delete=models.CASCADE)

