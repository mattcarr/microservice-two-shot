from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json
from django.http import JsonResponse
from common.json import ModelEncoder

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "href", "closet_name", "bin_number",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name", "picture_url", "manufacturer", "color", "bin",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder)
    else:
        content = json.loads(request.body)

        try:
            post_href = content["bin"]
            bin = BinVO.objects.get(href=post_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"Message": "Invalid bin id"}, status=400,)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeListEncoder, safe=False)

@require_http_methods(["DELETE"])
def api_show_shoe(request, pk):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})

@require_http_methods(["GET"])
def api_list_Bin(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
        return JsonResponse({"bins": bins}, encoder=BinVOEncoder)
