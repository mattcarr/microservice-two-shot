from django.db import models


class BinVO(models.Model):
    href = models.CharField(max_length=100, default=None)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField()


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField()
    bin = models.ForeignKey(BinVO, related_name="bins", on_delete=models.CASCADE)
