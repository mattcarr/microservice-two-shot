from django.urls import path, include
from .api_views import api_list_shoes, api_list_Bin, api_show_shoe


urlpatterns = [
    path('shoes/', api_list_shoes, name="api_list_shoes"),
    path('bins/', api_list_Bin, name="binVo_list"),
    path('shoes/<int:pk>/', api_show_shoe, name="shoe_detail"),
    path('shoes/<int:pk>/', api_show_shoe, name="delete_shoe"),

]
